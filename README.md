# README #

### WASD - для управления ###

### Здесь разрабатывается: ###

* Змейка 2д

### Список задач: ###

* К 7 мая: 
- Спавн игрока (15 минут)
- Управление (перемещение) (30 минут)
- Случайное появление элемента (20 минут)
- Счётчик очков (20 минут)
- Подбор элемента (20 минут)
- Засчёт очков (10 минут)
- Увеличение длины змейки (50 минут)

* К 14 мая: 
- Обработка столконовений (со стеной, с собой) (35 минут)
- Отображение результата счёта (25 минут)
- Рекордный (20 минут)
- Кнопка начать снова (20 минут)
- Отладка (оставшееся время)